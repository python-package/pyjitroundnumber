import unittest

from src.py_jit_round_number.jit_round_up_down import JitRoundNumber


class MyTestCase(unittest.TestCase):

    def setUp( self ) -> None:
        self.listNumber = [12.34567890, -12.34567890, 0.54321, -0.54321, 1.1, -1.5, 1.23, 1.543, 22.45, 1352]

    def testFailErrorExceptionRoundUp( self ):
        # TODO Arrange 'สำหรับกำหนดค่าเริ่มต้นของการทดสอบ'
        txt = 'a'
        lists = ['1,234.345', "10000000.0000009"]

        '''
        Test fail case 1 ใส่ค่าตัวแปร `txt`
        '''
        # TODO Act 'สำหรับ execute ส่วนที่ต้องการทดสอบหรือ business logic'
        jr_none = JitRoundNumber(None).round_up()
        jr_txt = JitRoundNumber(txt).round_up()
        jr_list = JitRoundNumber(lists).round_up()
        jr_list_index = JitRoundNumber(lists[0]).round_up()

        # TODO Assert 'สำหรับการตรวจสอบว่า ผลการทำงานตรงตามที่คาดหวังหรือไม่ ต้องมีทุก test นะขาดไม่ได้'
        self.assertIsInstance(obj=jr_txt, cls=str, msg=f"{jr_txt}")
        self.assertEqual(first="TypeError: must be real number, not str", second=jr_txt,
                         msg=f"{jr_txt} = TypeError: must be real number, not str")

        self.assertNotIsInstance(obj=jr_list_index, cls=list, msg=f"{jr_list}")
        self.assertEqual(first='TypeError: must be real number, not str', second=jr_list_index,
                         msg=f'{jr_list_index} = TypeError: must be real number, not str')

        self.assertNotIsInstance(obj=jr_list, cls=list, msg=f"{jr_list}")
        self.assertEqual(first='TypeError: must be real number, not list', second=jr_list,
                         msg=f"{jr_list} = TypeError: must be real number, not list")

        self.assertNotIsInstance(obj=jr_none, cls=int, msg=f"{jr_none}")  #
        self.assertIsNotNone(obj=jr_none,
                             msg=f"{jr_none} = TypeError: unsupported operand type(s) for *: 'NoneType' and 'int'")
        self.assertEqual(first="TypeError: unsupported operand type(s) for *: 'NoneType' and 'int'", second=jr_none,
                         msg=f"{jr_none} = TypeError: unsupported operand type(s) for *: 'NoneType' and 'int'")

    def testFailErrorExceptionRoundDown( self ):
        # TODO Arrange 'สำหรับกำหนดค่าเริ่มต้นของการทดสอบ'
        txt = 'a'
        lists = ['1,234.345', "10000000.0000009"]

        '''
        Test fail case 1 ใส่ค่าตัวแปร `txt`
        '''
        # TODO Act 'สำหรับ execute ส่วนที่ต้องการทดสอบหรือ business logic'
        jr_none = JitRoundNumber(None).round_down()
        jr_txt = JitRoundNumber(txt).round_down()
        jr_list = JitRoundNumber(lists).round_down()
        jr_list_index = JitRoundNumber(lists[0]).round_down()

        # TODO Assert 'สำหรับการตรวจสอบว่า ผลการทำงานตรงตามที่คาดหวังหรือไม่ ต้องมีทุก test นะขาดไม่ได้'
        self.assertIsInstance(obj=jr_txt, cls=str, msg=f"{jr_txt}")
        self.assertEqual(first="TypeError: must be real number, not str", second=jr_txt,
                         msg=f"{jr_txt} = TypeError: must be real number, not str")

        self.assertNotIsInstance(obj=jr_list_index, cls=list, msg=f"{jr_list}")
        self.assertEqual(first='TypeError: must be real number, not str', second=jr_list_index,
                         msg=f'{jr_list_index} = TypeError: must be real number, not str')

        self.assertNotIsInstance(obj=jr_list, cls=list, msg=f"{jr_list}")
        self.assertEqual(first='TypeError: must be real number, not list', second=jr_list,
                         msg=f"{jr_list} = TypeError: must be real number, not list")

        self.assertNotIsInstance(obj=jr_none, cls=int, msg=f"{jr_none}")  #
        self.assertIsNotNone(obj=jr_none,
                             msg=f"{jr_none} = TypeError: unsupported operand type(s) for *: 'NoneType' and 'int'")
        self.assertEqual(first="TypeError: unsupported operand type(s) for *: 'NoneType' and 'int'", second=jr_none,
                         msg=f"{jr_none} = TypeError: unsupported operand type(s) for *: 'NoneType' and 'int'")

    def testRoundUp( self ):
        # TODO Arrange 'สำหรับกำหนดค่าเริ่มต้นของการทดสอบ'
        number = self.listNumber

        ''' ทดนิยม 0 ตำแหน่ง และปัดขึ้น '''
        # TODO Act 'สำหรับ execute ส่วนที่ต้องการทดสอบหรือ business logic'
        jr = JitRoundNumber(number=number[0], decimals=0).round_up()

        # TODO Assert 'สำหรับการตรวจสอบว่า ผลการทำงานตรงตามที่คาดหวังหรือไม่ ต้องมีทุก test นะขาดไม่ได้'
        self.assertEqual(first=13.0, second=jr, msg=f"13.0 == {jr}")
        self.assertEqual(first=13, second=jr, msg=f"13 == {jr}")
        self.assertNotEqual(first=number[0], second=jr, msg=f"{number[0]} != {jr}")
        self.assertNotEqual(first=12.0, second=jr, msg=f"12.0 != {jr}")
        self.assertNotEqual(first=12, second=jr, msg=f"12 != {jr}")
        self.assertNotEqual(first=12.1, second=jr, msg=f"12.1 != {jr}")

        ''' ทดนิยม 1 ตำแหน่ง และปัดขึ้น '''
        # TODO Act 'สำหรับ execute ส่วนที่ต้องการทดสอบหรือ business logic'
        jr = JitRoundNumber(number=number[0], decimals=1).round_up()

        # TODO Assert 'สำหรับการตรวจสอบว่า ผลการทำงานตรงตามที่คาดหวังหรือไม่ ต้องมีทุก test นะขาดไม่ได้'
        self.assertEqual(first=12.4, second=jr, msg=f"12.4 == {jr}")
        self.assertNotEqual(first=12.5, second=jr, msg=f"12.5 == {jr}")
        self.assertNotEqual(first=12.6, second=jr, msg=f"12.6 == {jr}")
        self.assertNotEqual(first=number[0], second=jr, msg=f"{number[0]} != {jr}")
        self.assertNotEqual(first=12.0, second=jr, msg=f"12.0 != {jr}")
        self.assertNotEqual(first=12, second=jr, msg=f"12 != {jr}")
        self.assertNotEqual(first=12.34, second=jr, msg=f"12.34 != {jr}")

        ''' ทดนิยม 2 ตำแหน่ง และปัดขึ้น '''
        # TODO Act 'สำหรับ execute ส่วนที่ต้องการทดสอบหรือ business logic'
        jr = JitRoundNumber(number=number[0], decimals=2).round_up()

        # TODO Assert 'สำหรับการตรวจสอบว่า ผลการทำงานตรงตามที่คาดหวังหรือไม่ ต้องมีทุก test นะขาดไม่ได้'
        self.assertEqual(first=12.35, second=jr, msg=f"12.35 == {jr}")
        self.assertNotEqual(first=12.36, second=jr, msg=f"12.36 == {jr}")
        self.assertNotEqual(first=number[0], second=jr, msg=f"{number[0]} != {jr}")
        self.assertNotEqual(first=12.30, second=jr, msg=f"12.30 != {jr}")
        self.assertNotEqual(first=12.40, second=jr, msg=f"12.40 != {jr}")
        self.assertNotEqual(first=12.20, second=jr, msg=f"12.20 != {jr}")

        ''' ทดนิยม 3 ตำแหน่ง และปัดขึ้น '''
        # TODO Act 'สำหรับ execute ส่วนที่ต้องการทดสอบหรือ business logic'
        jr = JitRoundNumber(number=number[0], decimals=3).round_up()

        # TODO Assert 'สำหรับการตรวจสอบว่า ผลการทำงานตรงตามที่คาดหวังหรือไม่ ต้องมีทุก test นะขาดไม่ได้'
        self.assertEqual(first=12.346, second=jr, msg=f"12.346 == {jr}")
        self.assertNotEqual(first=12.345, second=jr, msg=f"12.345 == {jr}")
        self.assertNotEqual(first=number[0], second=jr, msg=f"{number[0]} != {jr}")
        self.assertNotEqual(first=12.340, second=jr, msg=f"12.340 != {jr}")
        self.assertNotEqual(first=12.350, second=jr, msg=f"12.340 != {jr}")

        ''' ทดนิยม 4 ตำแหน่ง และปัดขึ้น '''
        # TODO Act 'สำหรับ execute ส่วนที่ต้องการทดสอบหรือ business logic'
        jr = JitRoundNumber(number=number[0], decimals=4).round_up()

        # TODO Assert 'สำหรับการตรวจสอบว่า ผลการทำงานตรงตามที่คาดหวังหรือไม่ ต้องมีทุก test นะขาดไม่ได้'
        self.assertEqual(first=12.3457, second=jr, msg=f"12.3457 == {jr}")
        self.assertNotEqual(first=12.3460, second=jr, msg=f"12.3460 == {jr}")
        self.assertNotEqual(first=number[0], second=jr, msg=f"{number[0]} != {jr}")
        self.assertNotEqual(first=12.3450, second=jr, msg=f"12.3550 != {jr}")
        self.assertNotEqual(first=12.3440, second=jr, msg=f"12.3440 != {jr}")

    def testRoundDown( self ):
        # TODO Arrange 'สำหรับกำหนดค่าเริ่มต้นของการทดสอบ'
        number = self.listNumber

        ''' ทดนิยม 0 ตำแหน่ง และปัดขึ้น '''
        # TODO Act 'สำหรับ execute ส่วนที่ต้องการทดสอบหรือ business logic'
        jr = JitRoundNumber(number=number[0], decimals=0).round_down()

        # TODO Assert 'สำหรับการตรวจสอบว่า ผลการทำงานตรงตามที่คาดหวังหรือไม่ ต้องมีทุก test นะขาดไม่ได้'
        self.assertNotEqual(first=11, second=jr, msg=f"11 != {jr}")
        self.assertNotEqual(first=11.0, second=jr, msg=f"11.0 != {jr}")
        self.assertNotEqual(first=11.1, second=jr, msg=f"11.1 != {jr}")

        self.assertEqual(first=12.0, second=jr, msg=f"12.0 == {jr}")
        self.assertEqual(first=12, second=jr, msg=f"12 == {jr}")

        self.assertNotEqual(first=13, second=jr, msg=f"13 != {jr}")
        self.assertNotEqual(first=13.0, second=jr, msg=f"13.0 != {jr}")
        self.assertNotEqual(first=13.1, second=jr, msg=f"13.1 != {jr}")

        self.assertNotEqual(first=number[0], second=jr, msg=f"{number[0]} != {jr}")

        ''' ทดนิยม 1 ตำแหน่ง และปัดขึ้น '''
        # TODO Act 'สำหรับ execute ส่วนที่ต้องการทดสอบหรือ business logic'
        jr = JitRoundNumber(number=number[0], decimals=1).round_down()

        # TODO Assert 'สำหรับการตรวจสอบว่า ผลการทำงานตรงตามที่คาดหวังหรือไม่ ต้องมีทุก test นะขาดไม่ได้'
        self.assertNotEqual(first=11, second=jr, msg=f"11 != {jr}")
        self.assertNotEqual(first=11.3, second=jr, msg=f"11.3 != {jr}")
        self.assertNotEqual(first=11.34, second=jr, msg=f"11.34 != {jr}")
        self.assertNotEqual(first=11.35, second=jr, msg=f"11.35 != {jr}")

        self.assertNotEqual(first=12, second=jr, msg=f"12 == {jr}")
        self.assertEqual(first=12.3, second=jr, msg=f"12.3 == {jr}")
        self.assertNotEqual(first=12.4, second=jr, msg=f"12.4 == {jr}")
        self.assertNotEqual(first=12.34, second=jr, msg=f"12.34 != {jr}")
        self.assertNotEqual(first=12.45, second=jr, msg=f"12.45 == {jr}")

        self.assertNotEqual(first=13, second=jr, msg=f"13 != {jr}")
        self.assertNotEqual(first=13.0, second=jr, msg=f"13.0 == {jr}")
        self.assertNotEqual(first=13.3, second=jr, msg=f"13.3 != {jr}")
        self.assertNotEqual(first=13.4, second=jr, msg=f"13.4 == {jr}")
        self.assertNotEqual(first=13.34, second=jr, msg=f"13.34 != {jr}")
        self.assertNotEqual(first=13.35, second=jr, msg=f"13.35 != {jr}")

        self.assertNotEqual(first=number[0], second=jr, msg=f"{number[0]} != {jr}")

        ''' ทดนิยม 2 ตำแหน่ง และปัดขึ้น '''
        # TODO Act 'สำหรับ execute ส่วนที่ต้องการทดสอบหรือ business logic'
        jr = JitRoundNumber(number=number[0], decimals=2).round_down()

        # TODO Assert 'สำหรับการตรวจสอบว่า ผลการทำงานตรงตามที่คาดหวังหรือไม่ ต้องมีทุก test นะขาดไม่ได้'
        self.assertNotEqual(first=11, second=jr, msg=f"11 != {jr}")
        self.assertNotEqual(first=11.3, second=jr, msg=f"11.3 != {jr}")
        self.assertNotEqual(first=11.4, second=jr, msg=f"11.4 != {jr}")
        self.assertNotEqual(first=11.35, second=jr, msg=f"11.35 != {jr}")
        self.assertNotEqual(first=11.346, second=jr, msg=f"11.346 != {jr}")
        self.assertNotEqual(first=11.345, second=jr, msg=f"11.345 != {jr}")

        self.assertNotEqual(first=12, second=jr, msg=f"12 == {jr}")
        self.assertEqual(first=12.34, second=jr, msg=f"12.34 == {jr}")
        self.assertNotEqual(first=12.3, second=jr, msg=f"12.3 != {jr}")
        self.assertNotEqual(first=12.4, second=jr, msg=f"12.4 == {jr}")
        self.assertNotEqual(first=12.45, second=jr, msg=f"12.45 == {jr}")

        self.assertNotEqual(first=13, second=jr, msg=f"13 != {jr}")
        self.assertNotEqual(first=13.0, second=jr, msg=f"13.0 == {jr}")
        self.assertNotEqual(first=13.3, second=jr, msg=f"13.3 != {jr}")
        self.assertNotEqual(first=13.4, second=jr, msg=f"13.4 == {jr}")
        self.assertNotEqual(first=13.34, second=jr, msg=f"13.34 != {jr}")
        self.assertNotEqual(first=13.35, second=jr, msg=f"13.35 != {jr}")

        self.assertNotEqual(first=number[0], second=jr, msg=f"{number[0]} != {jr}")

        ''' ทดนิยม 3 ตำแหน่ง และปัดขึ้น '''
        # TODO Act 'สำหรับ execute ส่วนที่ต้องการทดสอบหรือ business logic'
        jr = JitRoundNumber(number=number[0], decimals=3).round_down()

        # TODO Assert 'สำหรับการตรวจสอบว่า ผลการทำงานตรงตามที่คาดหวังหรือไม่ ต้องมีทุก test นะขาดไม่ได้'
        self.assertNotEqual(first=11, second=jr, msg=f"11 != {jr}")
        self.assertNotEqual(first=11.3, second=jr, msg=f"11.3 != {jr}")
        self.assertNotEqual(first=11.4, second=jr, msg=f"11.4 != {jr}")
        self.assertNotEqual(first=11.35, second=jr, msg=f"11.35 != {jr}")
        self.assertNotEqual(first=11.346, second=jr, msg=f"11.346 != {jr}")
        self.assertNotEqual(first=11.345, second=jr, msg=f"11.345 != {jr}")
        self.assertNotEqual(first=11.3457, second=jr, msg=f"11.3457 != {jr}")

        self.assertNotEqual(first=12, second=jr, msg=f"12 == {jr}")
        self.assertEqual(first=12.345, second=jr, msg=f"12.34 == {jr}")
        self.assertNotEqual(first=12.3, second=jr, msg=f"12.3 != {jr}")
        self.assertNotEqual(first=12.4, second=jr, msg=f"12.4 == {jr}")
        self.assertNotEqual(first=12.456, second=jr, msg=f"12.456 == {jr}")

        self.assertNotEqual(first=13, second=jr, msg=f"13 != {jr}")
        self.assertNotEqual(first=13.0, second=jr, msg=f"13.0 == {jr}")
        self.assertNotEqual(first=13.3, second=jr, msg=f"13.3 != {jr}")
        self.assertNotEqual(first=13.4, second=jr, msg=f"13.4 == {jr}")
        self.assertNotEqual(first=13.34, second=jr, msg=f"13.34 != {jr}")
        self.assertNotEqual(first=13.345, second=jr, msg=f"13.345 != {jr}")
        self.assertNotEqual(first=13.35, second=jr, msg=f"13.35 != {jr}")
        self.assertNotEqual(first=13.356, second=jr, msg=f"13.356 != {jr}")

        self.assertNotEqual(first=number[0], second=jr, msg=f"{number[0]} != {jr}")

        ''' ทดนิยม 4 ตำแหน่ง และปัดขึ้น '''
        # TODO Act 'สำหรับ execute ส่วนที่ต้องการทดสอบหรือ business logic'
        jr = JitRoundNumber(number=number[0], decimals=4).round_down()

        # TODO Assert 'สำหรับการตรวจสอบว่า ผลการทำงานตรงตามที่คาดหวังหรือไม่ ต้องมีทุก test นะขาดไม่ได้'
        self.assertNotEqual(first=11, second=jr, msg=f"11 != {jr}")
        self.assertNotEqual(first=11.3, second=jr, msg=f"11.3 != {jr}")
        self.assertNotEqual(first=11.4, second=jr, msg=f"11.4 != {jr}")
        self.assertNotEqual(first=11.35, second=jr, msg=f"11.35 != {jr}")
        self.assertNotEqual(first=11.346, second=jr, msg=f"11.346 != {jr}")
        self.assertNotEqual(first=11.345, second=jr, msg=f"11.345 != {jr}")
        self.assertNotEqual(first=11.3457, second=jr, msg=f"11.3457 != {jr}")

        self.assertNotEqual(first=12, second=jr, msg=f"12 == {jr}")
        self.assertEqual(first=12.3456, second=jr, msg=f"12.34 == {jr}")
        self.assertNotEqual(first=12.3, second=jr, msg=f"12.3 != {jr}")
        self.assertNotEqual(first=12.4, second=jr, msg=f"12.4 == {jr}")
        self.assertNotEqual(first=12.4567, second=jr, msg=f"12.4567 == {jr}")
        self.assertNotEqual(first=12.4565, second=jr, msg=f"12.4565 == {jr}")

        self.assertNotEqual(first=13, second=jr, msg=f"13 != {jr}")
        self.assertNotEqual(first=13.0, second=jr, msg=f"13.0 == {jr}")
        self.assertNotEqual(first=13.3, second=jr, msg=f"13.3 != {jr}")
        self.assertNotEqual(first=13.4, second=jr, msg=f"13.4 == {jr}")
        self.assertNotEqual(first=13.34, second=jr, msg=f"13.34 != {jr}")
        self.assertNotEqual(first=13.345, second=jr, msg=f"13.345 != {jr}")
        self.assertNotEqual(first=13.35, second=jr, msg=f"13.35 != {jr}")
        self.assertNotEqual(first=13.356, second=jr, msg=f"13.356 != {jr}")
        self.assertNotEqual(first=13.3567, second=jr, msg=f"13.3567 != {jr}")
        self.assertNotEqual(first=13.3568, second=jr, msg=f"13.3568 != {jr}")
        self.assertNotEqual(first=13.3565, second=jr, msg=f"13.3565 != {jr}")

        self.assertNotEqual(first=number[0], second=jr, msg=f"{number[0]} != {jr}")


if __name__ == '__main__':
    unittest.main()
