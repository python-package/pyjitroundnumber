from src.py_jit_round_number.jit_round_up_down import JitRoundNumber

listNumber = [12.34567890, -12.34567890, 0.54321, -0.54321, 1.1, -1.5, 1.23, 1.543, 22.45, 1352]

''' round_up '''
print('Round Up')
i = 0
for j in range(0, 5):
    for l in listNumber:
        print(f"ทศนิยม {j} ตำแหน่ง : {JitRoundNumber(number=l, decimals=j).round_up()}")

    i += j
    if j <= i:
        print('\r\t\n')

print('=================')

''' round_down '''
print('Round Down')
i = 0
for j in range(0, 5):
    for l in listNumber:
        print(f"ทศนิยม {j} ตำแหน่ง : {JitRoundNumber(number=l, decimals=j).round_down()}")

    i += j
    if j <= i:
        print('\r\t\n')
